package com.xlongwei.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.noear.solon.Solon;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 文件上传工具类
 * 
 * @author xlongwei
 *
 */
@Slf4j
public class UploadUtil {
	/** SAVE-文件系统保存目录 URL-互联网访问网址 */
	public static final String SAVE = Solon.cfg().get("upload.save"), URL = Solon.cfg().get("upload.url");
	/** SAVE-临时文件保存目录 URL-临时文件访问网址 */
	public static final String SAVE_TEMP = SAVE + "temp/", URL_TEMP = URL + "temp/";
	/** 文件上传操作码 */
	public static final String DIRECT = "direct", TEMP = "temp", CONFIRM = "confirm", TRASH = "trash";
	/** 文件上传响应码 */
	public static final String DOMAIN = "domain", PATH = "path";

	/**
	 * 保存文件
	 * 
	 * @param is
	 * @param target
	 * @return
	 */
	public static boolean save(InputStream is, File target) {
		if (target.getParentFile().exists() == false) {
			target.getParentFile().mkdirs();
		}
		try {
			FileOutputStream out = new FileOutputStream(target, false);
			IoUtil.copy(is, out);
			out.close();
			log.info("save file to target:{}", target);
			return true;
		} catch (IOException e) {
			log.warn("fail to save file to target:{}", target);
		}
		return false;
	}

	/**
	 * 移动文件
	 * 
	 * @param path path/name.ext
	 * @param type confirm：temp->uploads，trash：uploads->trash
	 * @return
	 */
	public static boolean move(String path, String type) {
		if (StrUtil.isBlank(path)) {
			return false;
		}
		String from = null, to = null;
		if (CONFIRM.equals(type)) {
			from = SAVE_TEMP;
			to = SAVE;
		} else if (TRASH.equals(type)) {
			from = SAVE;
			to = SAVE + "/trash";
		} else {
			return false;
		}
		File source = new File(from, path);
		if (!source.exists() || source.isFile() == false) {
			log.info("move " + path + " from: " + from + " to: " + to + ", source not exist or not file.");
			return false;
		}
		File target = new File(to, path), targetParent = target.getParentFile();
		if (targetParent.exists() == false) {
			targetParent.mkdirs();
		}
		FileUtil.move(source, target, false);
		boolean b = target.exists();
		log.info("move:{} file:{} from:{} to:{}", b, path, from, to);
		return b;
	}
}
