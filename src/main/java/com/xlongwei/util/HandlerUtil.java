package com.xlongwei.util;

import org.noear.solon.core.handle.Context;

public class HandlerUtil {

    public static String getShowapiUserName() {
        Context ctx = Context.current();
        return ctx.header("showapi_userName");
    }

    public static boolean isShowapiRequest() {
        String showapiUserName = getShowapiUserName();
        return showapiUserName != null && showapiUserName.length() > 0;
    }
}
