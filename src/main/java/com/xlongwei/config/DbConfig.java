package com.xlongwei.config;

import javax.sql.DataSource;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DbConfig {
    public static DataSource ds;

    @Bean
    public DataSource db1(@Inject("${db}") HikariDataSource ds) {
        DbConfig.ds = ds;
        return ds;
    }
}
