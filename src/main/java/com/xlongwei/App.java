package com.xlongwei;

import org.noear.snack.ONode;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.web.cors.CrossHandler;

import com.xlongwei.util.HandlerUtil;

public class App {
    public static void main(String[] args) {
        Solon.start(App.class, args, app -> {
            // 跨域处理：支持Controller基类或方法注解@CrossOrigin(origins = "*")，也支持全局或路径跨域
            app.before(new CrossHandler().allowedOrigins("*"));
            Solon.context().beanAroundAdd(Mapping.class, inv -> {
                return changeResp(inv.invoke());
            });
        });
    }

    private static Object changeResp(Object o) {
        if (o instanceof ONode) {
            ONode n = (ONode) o;
            if (n.nodeData().object().isEmpty()) {
                n.set("error", "bad request");
            }
            if (HandlerUtil.isShowapiRequest()) {
                n.set("ret_code", "0");
            }
            return n;
        }
        return o;
    }
}
